const {
  addToCartService,
  getAllITemsInTheCart,
  deleteItemInCart,
  checkItemInCartService,
  updateItemInCartService,
} = require('../service/cart.service');
const {
  requestIsEmpty,
  fetchDataIsReturnsEmptyService,
} = require('../service/product.service');

const catchAsync = require('../utils/catchAsync');
const generateResponse = require('../utils/generateResponse');
const getJwtUser = require('../utils/getUserfronToken');

const HttpSuccessCode = require('../utils/HTTPsuccesscode');

// add, edit qauantity and delete items to cart
const addToCart = catchAsync(async (request, response) => {
  const { body } = request;

  requestIsEmpty(body);
  const decoded = await getJwtUser(request, response);

  fetchDataIsReturnsEmptyService(decoded);

  const newCartAdded = await addToCartService(body, decoded.id);

  return generateResponse(response, HttpSuccessCode.Created, newCartAdded);
});

// get all items of the user
const getItemsInTheCart = catchAsync(async (request, response) => {
  const { page, limit } = request.query;
  const decoded = await getJwtUser(request, response);

  fetchDataIsReturnsEmptyService(decoded);

  const userItemsInCart = await getAllITemsInTheCart(
    { userId: decoded.id },
    {
      page,
      limit,
    },
  );

  if (!userItemsInCart)
    return generateResponse(response, HttpSuccessCode.Accepted, {
      cart: userItemsInCart?.length,
    });

  return generateResponse(response, HttpSuccessCode.Accepted, userItemsInCart);
});

const deleteItemsToCart = catchAsync(async (request, response) => {
  const { productId } = request.params;
  const decoded = await getJwtUser(request, response);

  const deletedProduct = await deleteItemInCart(productId, decoded.id);

  return generateResponse(response, HttpSuccessCode.Accepted, deletedProduct);
});

const checkItemInCart = catchAsync(async (request, response) => {
  const { userId, productId } = request.body;

  const isProductInCart = await checkItemInCartService(userId, productId);

  return generateResponse(response, HttpSuccessCode.Accepted, isProductInCart);
});

const updateCartItems = catchAsync(async (request, response) => {
  const { productId } = request.params;
  const { body } = request;

  const decoded = await getJwtUser(request, response);

  fetchDataIsReturnsEmptyService(decoded);

  const updatedItem = await updateItemInCartService(
    decoded.id,
    productId,
    body,
  );

  return generateResponse(response, HttpSuccessCode.Accepted, updatedItem);
});

module.exports = {
  addToCart,
  getItemsInTheCart,
  deleteItemsToCart,
  checkItemInCart,
  updateCartItems,
};
