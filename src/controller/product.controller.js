const {
  saveProductstoDBService,
  getAllProductService,
  requestIsEmpty,
  getProductByCategoryService,
  getProductByIdService,
  fetchDataIsReturnsEmptyService,
  deleteItemService,
  getArchivedProducts,
  updateProductService,
  uploadImageService,
} = require('../service/product.service');
const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');
const generateResponse = require('../utils/generateResponse');
const HttpErrorCode = require('../utils/HTTPerrorcode');
const HttpSuccessCode = require('../utils/HTTPsuccesscode');

// create product to db
const createProduct = catchAsync(async (request, response) => {
  const { body } = request;

  // check if body is empty and throws error
  requestIsEmpty(body);

  // save product to database
  const newProduct = await saveProductstoDBService(body);

  return generateResponse(response, HttpSuccessCode.Created, newProduct);
});

// get all products
const getAllProducts = catchAsync(async (request, response) => {
  const { page, limit } = request.query;
  const products = await getAllProductService(
    {},
    {
      page,
      limit,
      select: '-createdAt -updatedAt +isActive',
    },
  );
  return generateResponse(response, HttpSuccessCode.OK, products);
});

// get all active products
const getAllActiveProducts = catchAsync(async (request, response, next) => {
  const activeProducts = await getArchivedProducts(true);

  fetchDataIsReturnsEmptyService(activeProducts, next);

  return generateResponse(response, HttpSuccessCode.Accepted, activeProducts);
});

// get all inactive products
const getAllInActiveProducts = catchAsync(async (request, response, next) => {
  const archivedProducts = await getArchivedProducts(false);
  fetchDataIsReturnsEmptyService(archivedProducts, next);

  return generateResponse(response, HttpSuccessCode.Accepted, archivedProducts);
});

// get products by category
const getProductByCategory = catchAsync(async (request, response, next) => {
  const { body } = request;

  // check if body is empty and throws error
  requestIsEmpty(body);

  // cets product
  const filteredProducts = getProductByCategoryService(body.category);
  fetchDataIsReturnsEmptyService(filteredProducts, next);

  return generateResponse(response, HttpSuccessCode.OK, filteredProducts);
});

// get products by id
const getProductById = catchAsync(async (request, response, next) => {
  const { id } = request.params;

  // check if id is empty and throws error
  requestIsEmpty(id);

  // fetch ID by service
  const product = await getProductByIdService(id);

  // check if fetch data is empty
  fetchDataIsReturnsEmptyService(product, next);
  return generateResponse(response, HttpSuccessCode.OK, product);
});

// update product
const updateProduct = catchAsync(async (request, response) => {
  const { id } = request.params;
  const { body } = request;

  const updatedProduct = await updateProductService(id, body);

  return generateResponse(response, HttpSuccessCode.Created, updatedProduct);
});

// delete a product
const deleteProduct = catchAsync(async (request, response) => {
  const { id } = request.params;

  // check if id is empty and throws error

  requestIsEmpty(id);

  const archivedUser = await deleteItemService(id);

  return generateResponse(response, HttpSuccessCode.OK, archivedUser);
});

// upload product
const uploadIamge = catchAsync(async (request, response) => {
  const { file } = request;
  const { productId } = request.params;

  if (!file) throw new AppError('No image found!', HttpErrorCode.BadRequest);

  const uploadedImage = await uploadImageService(productId, {
    image: file.path,
  });

  return generateResponse(response, HttpSuccessCode.OK, uploadedImage);
});

module.exports = {
  createProduct,
  getAllProducts,
  getProductByCategory,
  getProductById,
  deleteProduct,
  getAllInActiveProducts,
  getAllActiveProducts,
  updateProduct,
  uploadIamge,
};
