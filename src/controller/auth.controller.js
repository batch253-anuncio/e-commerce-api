// library
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcrypt');

// model
const User = require('../model/user.model');

// modules
const generateResponse = require('../utils/generateResponse');
const HttpSuccessCode = require('../utils/HTTPsuccesscode');
const {
  handleFilterRefreshToken,
  findUserAndCheckPasswordService,
  findUserByRefreshTokenService,
  handleAddRefreshToken,
  updateUserRefreshTokenService,
} = require('../service/auth.service');
const signTokens = require('../utils/signTokens');
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');
const HttpErrorCode = require('../utils/HTTPerrorcode');
const clearCookie = require('../utils/clearCookie');
const verifyJwt = require('../utils/verifyJwt');
const setRefreshToken = require('../utils/setRefreshToken');
const { omit } = require('lodash');

// sign in user using jwt
const signIn = catchAsync(async (request, response, next) => {
  const { cookies } = request;
  const { email, password } = request.body;

  const foundUser = await findUserAndCheckPasswordService(email, password);

  const { accessToken, refreshToken } = signTokens(foundUser);

  if (cookies.jwt) {
    await handleFilterRefreshToken(foundUser, cookies.jwt);
    clearCookie(request, response);
  } else {
    handleAddRefreshToken(foundUser, refreshToken);
  }

  await updateUserRefreshTokenService(foundUser);
  setRefreshToken(request, response, refreshToken);

  return generateResponse(response, HttpSuccessCode.OK, {
    id: foundUser._id,
    email: foundUser.email,
    firstName: foundUser.firstName,
    lastName: foundUser.lastName,
    image: foundUser.image,
    isAdmin: foundUser.isAdmin,
    accessToken,
  });
});

// gives new accesstoken and refreshtoken
const handleRefreshToken = catchAsync(async (request, response, next) => {
  const { jwt } = request.cookies;

  if (!jwt)
    return next(new AppError('Forbidden access!', HttpErrorCode.Forbidden));

  const refreshToken = jwt;

  // clearCookie(request, response);
  const foundUser = await findUserByRefreshTokenService(refreshToken);

  const refreshTokenDuration = process.env.JWT_REFRESHTOKEN_SECRET;

  const decoded = await verifyJwt(refreshToken, refreshTokenDuration);

  // Detected refresh token reuse!
  if (!foundUser) {
    const hackedUser = await User.findById(decoded.id)
      .select('+refreshToken')
      .exec();

    if (hackedUser) {
      hackedUser.refreshToken = [];
      await hackedUser.save();
    }
    return next(new AppError('Forbidden access!', HttpErrorCode.Forbidden));
  }

  // Remove the used refresh token
  const newRefreshTokenArray = foundUser.refreshToken.filter(
    (el) => el !== refreshToken,
  );

  const { accessToken, refreshToken: newRefreshToken } = signTokens(foundUser);

  foundUser.refreshToken = [...newRefreshTokenArray, newRefreshToken];
  await foundUser.save();

  setRefreshToken(request, response, newRefreshToken);

  const userDataToReturn = omit(foundUser.toJSON(), [
    'refreshToken',
    'createdAt',
    'updatedAt',
    '__v',
  ]);

  return generateResponse(response, HttpSuccessCode.OK, {
    ...userDataToReturn,
    accessToken,
  });
});

// clear the cookie
async function handleLogout(request, response) {
  // On client, also delete the accessToken
  const cookies = request.cookies;

  if (!cookies.jwt) return response.sendStatus(204); // No content
  const refreshToken = cookies.jwt;

  // // Is refreshToken in db?
  const foundUser = await User.findOne({ refreshToken })
    .select('+refreshToken')
    .exec();
  if (!foundUser) {
    clearCookie(request, response);
    return response.sendStatus(204);
  }

  // Delete refreshToken in db
  foundUser.refreshToken = foundUser.refreshToken.filter(
    (rt) => rt !== refreshToken,
  );
  await foundUser.save();

  clearCookie(request, response);
  return response.status(204).json({ message: 'Successfully logged out' });
}

module.exports = { signIn, handleRefreshToken, handleLogout };
