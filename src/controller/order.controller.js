/* eslint-disable prefer-const */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable no-case-declarations */

const {
  checkOutItemsOrderedService,
  getAllOrdersSerrvice,
} = require('../service/order.service');
const {
  requestIsEmpty,
  fetchDataIsReturnsEmptyService,
} = require('../service/product.service');
const Order = require('../model/order.model');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const catchAsync = require('../utils/catchAsync');
const generateResponse = require('../utils/generateResponse');
const getJwtUser = require('../utils/getUserfronToken');
const Cart = require('../model/cart.model');

const HttpSuccessCode = require('../utils/HTTPsuccesscode');
const AppError = require('../utils/appError');
const HttpErrorCode = require('../utils/HTTPerrorcode');

// User purchased the order
const checkOutOrder = catchAsync(async (request, response) => {
  const { body } = request;

  requestIsEmpty(body);
  const decoded = await getJwtUser(request, response);

  fetchDataIsReturnsEmptyService(decoded);

  const newCartAdded = await checkOutItemsOrderedService(decoded.id);

  return generateResponse(response, HttpSuccessCode.Created, newCartAdded);
});

const getAllOrderedItems = catchAsync(async (request, response) => {
  const decoded = await getJwtUser(request, response);
  fetchDataIsReturnsEmptyService(decoded);
  const orderHistory = await getAllOrdersSerrvice(decoded.id);
  return generateResponse(response, HttpSuccessCode.Created, orderHistory);
});

const handleCheckoutCompleted = async (data) => {
  const {
    metadata: { userId, items, buyItNow },
    payment_intent: paymentIntentId,
    amount_subtotal: subtotal,
    amount_total: total,
    customer_details: shipping,
    payment_status,
  } = data;

  const itemsArray = JSON.parse(items);

  const findUser = await Cart.findOne({ userId });

  if (!findUser)
    throw new AppError('Cant find user cart', HttpErrorCode.BadRequest);

  try {
    await Order.create({
      userId,
      paymentIntentId,
      products: itemsArray,
      subtotal,
      total,
      payment_status,
    });
    if (buyItNow === 'no') {
      await Cart.findOneAndDelete({ userId });
    }
  } catch (error) {
    console.log(error);
  }
};

const webhookCheckout = catchAsync(async (request, response) => {
  const sig = request.headers['stripe-signature'];
  let event;

  try {
    event = stripe.webhooks.constructEvent(
      request.rawBody,
      sig,
      process.env.WEB_HOOK_SECRET,
    );
  } catch (err) {
    console.log(err.message);
    return response.status(400).send(`Webhook Error: ${err.message}`);
  }

  if (event.type === 'checkout.session.completed') {
    const session = event.data.object;

    handleCheckoutCompleted(session);
  } else {
    console.log(`Unhandled event type ${event.type}`);
  }

  response.status(200).json({ received: true });
});

module.exports = { checkOutOrder, getAllOrderedItems, webhookCheckout };
