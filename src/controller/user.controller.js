const User = require('../model/user.model');
const {
  createUserService,
  getAllUserService,
  updateUserService,
  deleteUserService,
  myordersService,
  getAllUsersOrdersService,
  userUploadImage,
} = require('../service/user-service');
const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');
const checkForToken = require('../utils/checkForToken');

const generateResponse = require('../utils/generateResponse');
const getJwtUser = require('../utils/getUserfronToken');

const HttpErrorCode = require('../utils/HTTPerrorcode');
const HttpSuccessCode = require('../utils/HTTPsuccesscode');

// add a user using create
const signUp = catchAsync(async (request, response) => {
  const { body } = request;
  const authHeader = checkForToken(request);

  if (authHeader)
    throw new AppError(
      "Can't create user, already have an account",
      HttpErrorCode.Forbidden,
    );

  const user = await createUserService(body);

  return generateResponse(response, HttpSuccessCode.Created, user);
});

// Get all users
const getAllUsers = catchAsync(async (request, response) => {
  const users = await getAllUserService();
  return generateResponse(response, HttpSuccessCode.Created, users);
});

// Get a user

const getUser = catchAsync(async (request, response, next) => {
  const { id } = request.params;

  const user = await User.findById(id);
  return response.status(201).json({ data: user });
});

// update user
const updateUser = async (request, response) => {
  const { body } = request;
  const { id } = request.params;
  try {
    const user = await updateUserService(id, body);
    return generateResponse(response, HttpSuccessCode.Created, user);
  } catch (error) {
    response.status(HttpErrorCode.BadRequest).json({ message: error });
  }
};

// delete a user
const deleteUser = catchAsync(async (request, response) => {
  const { id } = request.params;

  const deleted = await deleteUserService(id);
  return generateResponse(
    response,
    HttpSuccessCode.Created,
    `${deleted.firstName} deleted`,
  );
});

// create admin user where only
const createAdmin = catchAsync(async (request, response) => {
  const { body } = request;

  const user = await createUserService(body);

  return generateResponse(response, HttpSuccessCode.Created, user);
});

// update user
const updateUserAsAdmin = catchAsync(async (request, response) => {
  const { body } = request;

  const user = await updateUserService(body);
  return generateResponse(response, HttpSuccessCode.Created, user);
});

// get all current user orders
const getMyOrders = catchAsync(async (request, response) => {
  const decoded = await getJwtUser(request, response);

  const myOrders = await myordersService(decoded.id);

  if (!myOrders)
    throw new AppError('No orders found', HttpErrorCode.BadRequest);

  return generateResponse(response, HttpSuccessCode.OK, myOrders);
});

// get all orders only admin can access

const getAllOrder = catchAsync(async (request, response) => {
  const allOrders = await getAllUsersOrdersService();

  if (!allOrders)
    throw new AppError('No Orders found', HttpErrorCode.BadRequest);

  return generateResponse(response, HttpSuccessCode.OK, allOrders);
});

// upload userImage
const uploadImage = catchAsync(async (request, response) => {
  const { file } = request;

  const decoded = await getJwtUser(request, response);

  const user = await userUploadImage(decoded.id, { image: file.path });

  return generateResponse(response, HttpSuccessCode.Created, user);
});

module.exports = {
  signUp,
  getAllUsers,
  updateUser,
  deleteUser,
  getUser,
  createAdmin,
  updateUserAsAdmin,
  getMyOrders,
  getAllOrder,
  uploadImage,
};
