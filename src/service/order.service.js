const Order = require('../model/order.model');
const Cart = require('../model/cart.model');
const AppError = require('../utils/appError');
const HttpErrorCode = require('../utils/HTTPerrorcode');

const checkOutItemsOrderedService = async (userId) => {
  const userCart = await Cart.findOne({ userId }).exec();
  const { id } = userCart;

  if (!userCart)
    throw new AppError(
      'No pending items in the cart',
      HttpErrorCode.BadRequest,
    );

  await Cart.findByIdAndDelete(id);

  return await Order.create({
    userId,
    cartId: userCart._id,
    items: userCart.cart[0].items,
    totalCost: userCart.cart[0].totalCost,
    datePurchased: userCart.cart[0].datePurchased,
  });
};

const getAllOrdersSerrvice = async (decoded) =>
  await Order.find({ userId: decoded }).exec();

module.exports = { checkOutItemsOrderedService, getAllOrdersSerrvice };
