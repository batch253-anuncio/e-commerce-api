const Cart = require('../model/cart.model');
const Product = require('../model/product.model');
const AppError = require('../utils/appError');
const checkMongooseId = require('../utils/checkValidMongooseID');
const HttpErrorCode = require('../utils/HTTPerrorcode');
const { fetchDataIsReturnsEmptyService } = require('./product.service');

const addToCartService = async (body, decoded) => {
  const { productId: id } = body[0];

  checkMongooseId(id);

  const userCart = await Cart.findOne({ userId: decoded }).exec();
  const validProduct = await Product.findById(id).select('+isActive').exec();

  if (!validProduct?.isActive)
    throw new AppError(
      'Product Id not found in the database or it is inActive',
      HttpErrorCode.BadRequest,
    );

  if (userCart) {
    body.forEach((item) => {
      const index = userCart.cart[0].items.findIndex(
        (el) => el.productId === item.productId,
      );

      if (index !== -1) {
        if (item.quantity === 0)
          userCart.cart[0].items = userCart.cart[0].items.filter(
            (el) => el.productId !== item.productId,
          );
        else if (item.quantity < userCart.cart[0].items[index].quantity)
          userCart.cart[0].items[index].quantity = item.quantity;
        else userCart.cart[0].items[index].quantity = item.quantity;
      } else {
        if (!item.quantity)
          throw new AppError(
            `${item.title} did not have any quantity`,
            HttpErrorCode.Conflict,
          );
        else userCart.cart[0].items.push(item);
      }
    });
    return await userCart.save();
  } else {
    body.forEach((item) => {
      if (item.quantity === 0)
        throw new AppError(
          `${item.title} did not have any quantity`,
          HttpErrorCode.Conflict,
        );
    });

    return await Cart.create({
      userId: decoded,
      cart: [
        {
          items: body,
        },
      ],
    });
  }
};

const getAllITemsInTheCart = async (filter, paginationOptions) =>
  await Cart.paginate(filter, paginationOptions);

const deleteItemInCart = async (id, userId) => {
  const userCart = await Cart.findOne({ userId }).exec();

  fetchDataIsReturnsEmptyService(userCart);

  userCart.cart[0].items = userCart.cart[0].items.filter(
    (item) => item.productId !== id,
  );

  return await userCart.save();
};

const checkItemInCartService = async (userId, productId) => {
  const userCart = await Cart.findOne({ userId }).exec();

  if (userCart) {
    const index = userCart.cart[0].items.findIndex(
      (item) => item.productId === productId,
    );

    if (index !== -1) return true;
    else return false;
  }
  return false;
};

const updateItemInCartService = async (userId, productId, body) => {
  const userCart = await Cart.findOne({ userId }).exec();
  const { quantity } = body;

  if (!userCart)
    throw new AppError('Item not found in cart', HttpErrorCode.BadRequest);

  if (quantity <= 0) {
    userCart.cart[0].items = userCart.cart[0].items.filter(
      (item) => item.productId !== productId,
    );
  } else {
    const findIndex = userCart.cart[0].items.findIndex(
      (item) => item.productId === productId,
    );

    if (findIndex !== -1) {
      userCart.cart[0].items[findIndex].quantity = quantity;
    }
  }

  return userCart.save();
};

module.exports = {
  addToCartService,
  getAllITemsInTheCart,
  deleteItemInCart,
  checkItemInCartService,
  updateItemInCartService,
};
