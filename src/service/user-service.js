const { omit } = require('lodash');

const Order = require('../model/order.model');
const User = require('../model/user.model');
const AppError = require('../utils/appError');
const checkMongooseId = require('../utils/checkValidMongooseID');
const HttpErrorCode = require('../utils/HTTPerrorcode');

const createUserService = async (input) => {
  const user = await User.create(input);
  const removedProps = omit(user.toJSON(), [
    'password',
    'refreshToken',
    'isActive',
    '__v',
  ]);

  return removedProps;
};

const getAllUserService = async () => {
  const user = await User.find().exec();

  return user;
};

const updateUserService = async (body) => {
  const { userId: id } = body;

  checkMongooseId(id);

  const updatedUser = await User.findByIdAndUpdate(id, body, {
    new: true,
  }).select('+isAdmin');
  return updatedUser;
};

const deleteUserService = async (id) => {
  checkMongooseId(id);
  return await User.findByIdAndDeleted(id);
};

const getUserByIDService = async (id) => await User.findById(id).exec();

const myordersService = async (userId) => {
  return await Order.find({ userId }).exec();
};

const getAllUsersOrdersService = async () => await Order.find().exec();

const userUploadImage = async (id, payload) => {
  const updatedUser = await User.findByIdAndUpdate(id, payload, {
    new: true,
  }).exec();

  if (updatedUser === null)
    throw new AppError('User not found', HttpErrorCode.NotFound);

  return updatedUser;
};

module.exports = {
  createUserService,
  getAllUserService,
  updateUserService,
  deleteUserService,
  getUserByIDService,
  myordersService,
  getAllUsersOrdersService,
  userUploadImage,
};
