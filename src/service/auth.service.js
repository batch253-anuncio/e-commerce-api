const User = require('../model/user.model');
const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');
const HttpErrorCode = require('../utils/HTTPerrorcode');

const findUserAndCheckPasswordService = catchAsync(async (email, password) => {
  const user = await User.findOne({ email })
    .select('+password +refreshToken +isAdmin')
    .exec();

  if (!user || !(await user.comparePassword(password, user.password)))
    throw new AppError(
      'Incorrect email or password',
      HttpErrorCode.Unauthorized,
    );

  return user;
});

const handleFilterRefreshToken = async (user, jwt) => {
  const foundToken = await User.findOne({ refreshToken: jwt }).exec();

  if (!foundToken) {
    user.refreshToken.filter((el) => el !== jwt);
    return;
  }

  user.refreshToken = [];
};

const handleAddRefreshToken = (user, newRefreshToken) => {
  return (user.refreshToken = [...user.refreshToken, newRefreshToken]);
};

const findUserByRefreshTokenService = async (refreshToken) => {
  const foundUser = await User.findOne({ refreshToken })
    .select('+refreshToken +isAdmin')
    .exec();

  return foundUser;
};

const updateUserRefreshTokenService = async (user) => {
  return await user.save();
};

module.exports = {
  findUserAndCheckPasswordService,
  findUserByRefreshTokenService,
  handleFilterRefreshToken,
  handleAddRefreshToken,
  updateUserRefreshTokenService,
};
