const signJwt = require('./signJWT');

const signTokens = (user) => {
  const id = user._id.toHexString();
  const isAdmin = user.isAdmin;
  const accessToken = signJwt(
    {
      id,
      isAdmin,
    },
    'access',
  );

  const refreshToken = signJwt(
    {
      id,
    },
    'refresh',
  );

  return { accessToken, refreshToken };
};

module.exports = signTokens;
