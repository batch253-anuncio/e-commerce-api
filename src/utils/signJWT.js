const jwt = require('jsonwebtoken');

const signJwt = (payload, type) => {
  const accessTokenSecret = process.env.JWT_ACCESSTOKEN_SECRET;
  const refreshTokenSecret = process.env.JWT_REFRESHTOKEN_SECRET;
  const accessTokenDuration = process.env.ACCESS_TOKEN_DURATION;
  const refreshTokenDuration = process.env.REFRESH_TOKEN_DURATION;

  const isAccess = type === 'access';
  const expiresIn = isAccess ? accessTokenDuration : refreshTokenDuration;
  const secret = isAccess ? accessTokenSecret : refreshTokenSecret;

  return jwt.sign(payload, secret, {
    expiresIn,
  });
};

module.exports = signJwt;
