const mongoose = require('mongoose');
const AppError = require('./appError');
const HttpErrorCode = require('./HTTPerrorcode');

const checkMongooseId = (id) => {
  if (!mongoose.Types.ObjectId.isValid(id))
    throw new AppError('This is not a valid id', HttpErrorCode.BadRequest);
};

module.exports = checkMongooseId;
