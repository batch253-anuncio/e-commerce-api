const verifyJwt = require('./verifyJwt');

const getJwtUser = async (request, response) => {
  const AccessSecret = process.env.JWT_ACCESSTOKEN_SECRET;
  const authHeader =
    request.headers.authorization || request.headers.Authorization;
  if (!authHeader?.startsWith('Bearer')) return response.sendStatus(401);
  const token = authHeader.split(' ')[1];

  return await verifyJwt(token, AccessSecret);
};

module.exports = getJwtUser;
