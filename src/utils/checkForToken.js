const checkForToken = (request) => {
  const authHeader =
    request.headers.authorization || request.headers.Authorization;
  return authHeader;
};

module.exports = checkForToken;
