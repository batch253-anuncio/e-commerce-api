const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const app = require('./app');
const connectDB = require('./utils/connect');
const logger = require('./utils/logger');

process.on('uncaughtException', (error) => {
  logger.error('UNHANDLED EXCEPTION! Shutting down...');
  logger.error(error.name, error.message);
  process.exit(1);
});

const PORT = +(process.env.PORT || 1337);

connectDB();

mongoose.connection.once('open', () => {
  app
    .listen(PORT, () =>
      logger.info(`Server running at http://localhost:${PORT}`),
    )
    .on('error', () => {
      process.exit(1);
    });
});

mongoose.connection.on('error', (error) => {
  logger.error(
    `${error.no}: ${error.code}\t${error.syscall}\t${error.hostname}`,
    'mongoErrLog.log',
  );
});
