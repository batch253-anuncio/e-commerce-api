const { Schema, model } = require('mongoose');
const paginate = require('mongoose-paginate-v2');

const cartSchema = new Schema(
  {
    userId: {
      type: String,
    },

    cart: [
      {
        items: [
          {
            _id: false,
            productId: {
              type: String,
            },
            title: {
              type: String,
            },
            category: {
              type: String,
            },
            description: {
              type: String,
            },
            price: {
              type: Number,
            },
            image: {
              type: String,
              default: '',
            },
            quantity: {
              type: Number,
            },

            subTotal: {
              type: Number,
            },
          },
        ],
        _id: false,
        totalCost: {
          type: Number,
        },

        datePurchased: {
          type: Date,
          default: Date.now,
        },
      },
    ],
  },
  {
    timestamps: true,
  },
);

// before saving in the database execute this middleware
cartSchema.pre('save', function (next) {
  let totalCost = 0;
  this.cart.forEach((cart) => {
    cart.items.forEach((item) => {
      item.subTotal = Math.round(item.price * item.quantity * 100) / 100;
      totalCost += item.subTotal;
    });
    cart.totalCost = Math.round(totalCost * 100) / 100;
    totalCost = 0;
  });
  next();
});
cartSchema.plugin(paginate);
const Cart = model('Cart', cartSchema);
module.exports = Cart;
