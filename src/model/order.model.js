const { Schema, model } = require('mongoose');

const orderSchema = new Schema(
  {
    userId: {
      type: String,
    },

    paymentIntentId: {
      type: String,
    },

    products: [
      {
        productId: {
          type: String,
        },
        title: {
          type: String,
        },
        image: {
          type: String,
        },
        price: {
          type: Number,
        },
        quantity: {
          type: Number,
        },
      },
    ],
    subtotal: {
      type: Number,
    },

    total: {
      type: Number,
    },
    delivery_status: { type: String, default: 'pending' },
    payment_status: { type: String },
  },
  {
    timestamps: true,
  },
);

const Order = model('Order', orderSchema);
module.exports = Order;
