const { Schema, model } = require('mongoose');

const bcrypt = require('bcrypt');
const AppError = require('../utils/appError');
const HttpErrorCode = require('../utils/HTTPerrorcode');

const userSchema = new Schema(
  {
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
      index: 1,
    },
    image: {
      type: String,
      default: null,
    },
    password: {
      type: String,
      select: false,
    },
    isAdmin: {
      type: Boolean,
      default: false,
      select: false,
    },
    refreshToken: {
      type: [String],
      select: false,
      default: [],
    },
    isActive: {
      type: Boolean,
      default: true,
      select: false,
    },
    passwordChangedAt: {
      type: Date,
    },
    passwordResetToken: {
      type: String,
    },
    passwordResetExpires: {
      type: Date,
    },
  },
  {
    timestamps: true,
  },
);

// pre middleware to hash the password before saving into the DB
userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(12);
  const hash = await bcrypt.hash(this.password, salt);

  this.password = hash;

  return next();
});

userSchema.post('save', function (error, doc, next) {
  if (error.name === 'MongoServerError' && error.code === 11000) {
    next(
      new AppError(
        `We found a ducpliacte user for ${this.email}`,
        HttpErrorCode.Conflict,
      ),
    );
  } else {
    next();
  }
});

userSchema.methods.changedPasswordAfter = function (JWTTimestamp) {
  if (this.passwordChangedAt) {
    const changedTimestamp = this.passwordChangedAt.getTime() / 1000;

    if (typeof JWTTimestamp === 'number')
      return changedTimestamp > JWTTimestamp;
  }
  return false;
};

userSchema.methods.comparePassword = async function (
  candidatePassword,
  userPassword,
) {
  return await bcrypt.compare(candidatePassword, userPassword);
};

const User = model('User', userSchema);
module.exports = User;
