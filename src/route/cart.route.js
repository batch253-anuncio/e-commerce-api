const cartRouter = require('express').Router();

const cartController = require('../controller/cart.controller');
const {
  addToCartValidation,
  deleteValidator,
} = require('../validation/cart.validation');
const validate = require('../middleware/validate');
const userOnly = require('../middleware/userOnly');

cartRouter.use(userOnly);
cartRouter
  .route('/')
  .post(validate(addToCartValidation), cartController.addToCart)
  .delete(validate(deleteValidator), cartController.deleteItemsToCart)
  .get(cartController.getItemsInTheCart);

cartRouter.get('/checkItemInCart');

cartRouter.patch('/:productId', cartController.updateCartItems);
cartRouter.delete('/:productId', cartController.deleteItemsToCart);

module.exports = cartRouter;
