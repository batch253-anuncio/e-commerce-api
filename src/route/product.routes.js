// library
const productRouter = require('express').Router();

// module
const productController = require('../controller/product.controller');
const { uploadSingleImage } = require('../middleware/multer');
// const { uploadSingleImage } = require('../middleware/multer');
const validate = require('../middleware/validate');
const adminAccessOnly = require('../middleware/verifyRoles');
const {
  createProductValidator,
  updateProductValidator,
  archiveValidator,
} = require('../validation/product.validation');

productRouter.route('/').get(productController.getAllProducts);
productRouter.get('/getAllActive', productController.getAllActiveProducts);
productRouter.get('/getAllInActive', productController.getAllInActiveProducts);
productRouter.get('/:id', productController.getProductById);

// productRouter.post('/upload', uploadSingleImage);

productRouter.use(adminAccessOnly);
productRouter.post(
  '/',
  validate(createProductValidator),
  productController.createProduct,
);

productRouter.patch(
  '/uploads/:productId?',
  uploadSingleImage,
  productController.uploadIamge,
);

productRouter
  .route('/:id')
  .patch(validate(updateProductValidator), productController.updateProduct)
  .delete(validate(archiveValidator), productController.deleteProduct);

productRouter.patch(
  '/:id/archive',
  validate(updateProductValidator),
  productController.updateProduct,
);

module.exports = productRouter;
