const userRouter = require('express').Router();

const validate = require('../middleware/validate');
const {
  createUserValidator,
  updateUserValidator,
  deleteUserValdiator,
  getUserValidator,
  createAdminValidator,
  updateUserAsAdminValidator,
} = require('../validation/user.validation');
const userController = require('../controller/user.controller');
const authController = require('../controller/auth.controller');

const verifyToken = require('../middleware/verifyToken');
const adminAccessOnly = require('../middleware/verifyRoles');
const userOnly = require('../middleware/userOnly');
const { signInValidator } = require('../validation/auth.validation');
const { uploadUserImage } = require('../middleware/userUploadImage');

userRouter.post(
  '/signup',
  validate(createUserValidator),
  userController.signUp,
);
userRouter.get('/', userController.getAllUsers);

userRouter.post('/signin', validate(signInValidator), authController.signIn);
userRouter.get('/signout', authController.handleLogout);
userRouter.get('/refresh', authController.handleRefreshToken);

userRouter.post(
  '/createAdmin',
  adminAccessOnly,
  validate(createAdminValidator),
  userController.createAdmin,
);
userRouter.patch(
  '/createAdmin',
  adminAccessOnly,
  validate(updateUserAsAdminValidator),
  userController.updateUserAsAdmin,
);

userRouter.use(verifyToken);

userRouter.get('/orders', adminAccessOnly, userController.getAllOrder);
userRouter.patch('/uploadImage', uploadUserImage, userController.uploadImage);
userRouter.get('/myOrders', userOnly, userController.getMyOrders);
userRouter
  .route('/:id')
  .get(validate(getUserValidator), userController.getUser)
  .put(validate(updateUserValidator))
  .delete(validate(deleteUserValdiator));

module.exports = userRouter;
