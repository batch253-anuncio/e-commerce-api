/* eslint-disable prefer-const */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable no-case-declarations */
const express = require('express');
const orderRouter = express.Router();

const {
  checkOutOrder,
  webhookCheckout,
  getAllOrderedItems,
} = require('../controller/order.controller');
const userOnly = require('../middleware/userOnly');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const dotenv = require('dotenv');
dotenv.config();

const createLineItems = (cartItems) => {
  return cartItems.map((item) => {
    return {
      price_data: {
        currency: 'usd',
        product_data: {
          name: item.title,
          images: [item.image],
        },
        unit_amount: item.price * 100,
      },
      quantity: item.quantity,
    };
  });
};

// Create a Stripe checkout session
orderRouter.post('/create-checkout-session', async (req, res) => {
  const line_items = createLineItems(req.body.cartItems, req);

  try {
    const session = await stripe.checkout.sessions.create({
      line_items,
      mode: 'payment',
      metadata: {
        userId: req.body.userId,
        buyItNow: req.body.buyItNow,
        items: JSON.stringify(req.body.cartItems),
      },
      shipping_address_collection: {
        allowed_countries: ['US', 'CA'],
      },
      shipping_options: [
        {
          shipping_rate_data: {
            type: 'fixed_amount',
            fixed_amount: { amount: 0, currency: 'usd' },
            display_name: 'Free shipping',
            delivery_estimate: {
              minimum: { unit: 'business_day', value: 5 },
              maximum: { unit: 'business_day', value: 7 },
            },
          },
        },
      ],
      client_reference_id: req.body.userId,
      success_url: `${process.env.CLIENT_URL_SUCESS}/`,
      cancel_url: `${process.env.CLIENT_URL_SUCESS}/cart`,
    });
    res.send({ url: session.url });
  } catch (error) {
    console.log(error);
  }

  // Send the session URL back to the client
});

// Handle the checkout.session.completed webhook event
orderRouter.post('/webhook', webhookCheckout);
// Define the webhook endpoint for Stripe to send events to

orderRouter.use(userOnly);

orderRouter.route('/').get(getAllOrderedItems).post(checkOutOrder);

// Define the line items based on the items in the cart

module.exports = orderRouter;
