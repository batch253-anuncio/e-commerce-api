const { CloudinaryStorage } = require('multer-storage-cloudinary');
const multer = require('multer');

const AppError = require('../utils/appError');
const HttpErrorCode = require('../utils/HTTPerrorcode');

const cloudinary = require('../config/cloudinary');

const storage = new CloudinaryStorage({
  cloudinary,
  params: async (req, file) => {
    return {
      folder: 'product-images',
      // Convert image to WebP format and optimize
      format: 'webp',
      quality: 'auto',
      width: 500, // set your desired width here
    };
  },
});

const upload = multer({
  storage,
  fileFilter: (_, file, cb) => {
    if (!file.mimetype)
      cb(new AppError('No image found!', HttpErrorCode.BadRequest));

    if (file.mimetype.startsWith('image')) {
      cb(null, true);
    } else {
      cb(
        new AppError(
          'Not an image! Only images are allowed',
          HttpErrorCode.BadRequest,
        ),
      );
    }
  },
});

module.exports.uploadSingleImage = upload.single('image');
