const allowedOrigins = require('../config/allowedOrigins');

const credentials = (request, response, next) => {
  const origin = request.headers.origin;
  if (origin && allowedOrigins.includes(origin)) {
    response.setHeader('Access-Control-Allow-Credentials', '');
  }
  next();
};

module.exports = credentials;
