const allowedOrigins = [
  'http://127.0.0.1:5500',
  'http://localhost:3500',
  'http://localhost:3000',
  'http://localhost:5173',
  'https://finlay-ecommerce-app.vercel.app',
  'https://finlay-ecommerce-app.vercel.app/',
  'finlay-ecommerce-app.vercel.app',
];

module.exports = allowedOrigins;
