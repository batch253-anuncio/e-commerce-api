const allowedOrigins = require('./allowedOrigins');

const customOrigin = (origin, callback) => {
  if (!origin || allowedOrigins.indexOf(origin) !== -1) {
    callback(null, true);
  } else {
    callback(new Error('Not allowed by CORS'), false);
  }
};

const corsOptions = {
  origin: customOrigin,
  optionsSuccessStatus: 200,
  credentials: true,
};

module.exports = corsOptions;
