const z = require('zod');

const signInValidator = z.object({
  body: z.object({
    email: z
      .string({
        required_error: 'Email is required',
      })
      .email('Invalid email inputted'),

    password: z
      .string({
        required_error: 'Password is required',
      })
      .min(1, 'Password invalid'),
  }),
});

module.exports = { signInValidator };
