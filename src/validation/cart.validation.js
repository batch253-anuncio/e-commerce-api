const z = require('zod');

const addToCartValidation = z.object({
  body: z.array(
    z.object({
      productId: z
        .string({
          required_error: 'Prodcut Id is required',
        })
        .min(1, 'Prodcut Id is required'),
      title: z
        .string({
          required_error: 'Title is required',
        })
        .min(1, 'Title is required'),
      price: z
        .number({
          required_error: 'price is required',
        })
        .min(1, 'Price must not be empty'),
      quantity: z
        .number({
          required_error: 'quantity is required',
        })
        .min(1, 'Quantity must not be empty'),
    }),
  ),
});

const deleteValidator = z.object({
  params: z.object({
    productId: z
      .string({
        required_error: 'ProductID is required',
      })
      .min(1, 'ProductID is required'),
  }),
});
module.exports = {
  addToCartValidation,
  deleteValidator,
};
