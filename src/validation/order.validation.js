const z = require('zod');

const orderValidation = z.object({
  body: z.array(
    z.object({
      productId: z
        .string({
          required_error: 'ProductId is required',
        })
        .min(1, 'ProductId must not be an empty string'),
      title: z
        .string({
          required_error: 'Title is required',
        })
        .min(1, 'Title must not be an empty string'),
      price: z
        .number({
          required_error: 'price is required',
        })
        .min(1, 'Price must not be an empty'),
      quantity: z
        .number({
          required_error: 'quantity is required',
        })
        .min(1, 'Quantity must not be empty'),
    }),
  ),
});

const updateProductValidator = z.object({
  params: z.object({
    id: z.string({
      required_error: 'UserId is required',
    }),
  }),
  body: z.object({
    title: z.string().optional(),
    category: z.string().optional(),
    description: z.string().optional(),
    stock: z.number().optional(),
    isActive: z.boolean().optional(),
  }),
});

const archiveValidator = z.object({
  params: z.object({
    id: z.string({
      required_error: 'UserId is required',
    }),
  }),
});
module.exports = {
  orderValidation,
  updateProductValidator,
  archiveValidator,
};
