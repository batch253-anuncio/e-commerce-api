const z = require('zod');

const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/;

const createUserValidator = z.object({
  body: z.object({
    firstName: z
      .string({
        required_error: 'Name is required',
      })
      .min(1, 'firstName must not be empty'),
    lastName: z
      .string({
        required_error: 'Name is required',
      })
      .min(1, 'lastName must not be empty'),
    email: z
      .string({
        required_error: 'Email is required',
      })
      .email('Email is not valid'),
    password: z
      .string({
        required_error: 'Password is required',
      })
      .min(12)
      .regex(
        passwordRegex,
        'Password must meet the criteria for password strength.',
      ),

    image: z.string().optional(),
    roles: z.array().optional(),
  }),
});

const getUserValidator = z.object({
  params: z.object({
    id: z
      .string({
        required_error: 'UserId is required',
      })
      .min(1, 'UserId msut not be empty'),
  }),
});

const deleteUserValdiator = z.object({
  params: z.object({
    id: z
      .string({
        required_error: 'UserId is required',
      })
      .min(1, 'UserId msut not be empty'),
  }),
});

const updateUserValidator = z.object({
  params: z.object({
    id: z
      .string({
        required_error: 'UserId is required',
      })
      .min(1, 'UserId msut not be empty'),
  }),
  body: z.object({
    firstName: z.string().min(1).optional(),
    lastName: z.string().min(1).optional(),
    email: z.string().email('Please enter a valid email.').optional(),
    password: z
      .string()
      .regex(
        passwordRegex,
        'Password must meet the criteria for password strength.',
      )
      .optional(),
    isAdmin: z.boolean().optional(),

    image: z.string().optional(),
    roles: z.array().optional(),
  }),
});

const updateUserAsAdminValidator = z.object({
  body: z.object({
    userId: z.string().min(1, 'UserId must not be empty'),
    isAdmin: z.boolean(),
  }),
});

const createAdminValidator = z.object({
  body: z.object({
    firstName: z
      .string({
        required_error: 'Name is required',
      })
      .min(1, 'firstName must not be empty'),
    lastName: z
      .string({
        required_error: 'Name is required',
      })
      .min(1, 'lastName must not be empty'),
    email: z
      .string({
        required_error: 'Email is required',
      })
      .email('Email is not valid'),
    password: z
      .string({
        required_error: 'Password is required',
      })
      .min(12)
      .regex(
        passwordRegex,
        'Password must meet the criteria for password strength.',
      ),
    isAdmin: z.boolean(),
    image: z.string().min(1, 'Image link must not be empty').optional(),
    roles: z.array().optional(),
  }),
});

module.exports = {
  createUserValidator,
  updateUserValidator,
  deleteUserValdiator,
  getUserValidator,
  updateUserAsAdminValidator,
  createAdminValidator,
};
