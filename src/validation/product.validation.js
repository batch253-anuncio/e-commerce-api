const z = require('zod');

const createProductValidator = z.object({
  body: z.object({
    title: z
      .string({
        required_error: 'Title is required',
      })
      .min(1, 'Product Title must not be empty'),
    category: z
      .string({
        required_error: 'Category is required',
      })
      .min(1, 'Category must not be empty'),
    description: z
      .string({
        required_error: 'Item description is required',
      })
      .min(1, 'Description must not be empty'),
    price: z.number('Price is required').min(1, 'Price must not be 0'),
    stock: z.number().min(1, 'Stock must not be empty').optional(),
  }),
});

const updateProductValidator = z.object({
  params: z.object({
    id: z
      .string({
        required_error: 'UserId is required',
      })
      .min(1, 'ProductId must not be empty'),
  }),
  body: z.object({
    title: z.string().min(1, 'Product Title must not be empty').optional(),
    category: z
      .string()
      .min(1, 'Product Category must not be empty')
      .optional(),
    description: z
      .string()
      .min(1, 'Product description must not be empty')
      .optional(),
    price: z
      .number('Price is required')
      .min(1, 'Price must not be 0')
      .optional(),
    stock: z.number().min(1, 'Stock must not be empty').optional(),
    image: z.string().min(1, 'Image is required').optional(),
    isActive: z.boolean().optional(),
  }),
});

const archiveValidator = z.object({
  params: z.object({
    id: z
      .string({
        required_error: 'UserId is required',
      })
      .min(1, 'UserID must not be empty'),
  }),
});

const getProductValidator = z.object({
  params: z.object({
    id: z.string().min(1, 'ProductId must not be empty'),
  }),
});
module.exports = {
  createProductValidator,
  updateProductValidator,
  archiveValidator,
  getProductValidator,
};
