// libraries
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
// const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const hpp = require('hpp');

// modules
const userRouter = require('./route/user.routes');
const errorController = require('./controller/error.controller');
const productRouter = require('./route/product.routes');

const dotenv = require('dotenv');
const cartRouter = require('./route/cart.route');
const orderRouter = require('./route/order.route');
const corsOptions = require('./config/corsOptions');

const credentials = require('./middleware/credentials');

dotenv.config();

const app = express();

// middlewares

app.use(
  express.json({
    limit: '5mb',
    verify: (req, res, buf) => {
      req.rawBody = buf.toString();
    },
  }),
);

app.use(credentials);
app.use(cors(corsOptions));
app.use(morgan('dev'));
app.use(cookieParser());
app.use(helmet());
app.use(express.urlencoded({ extended: false }));
app.use(mongoSanitize());
app.use(
  hpp({
    whitelist: [],
  }),
);
// app.use(
//   '/api',
//   rateLimit({
//     max: 100,
//     windowMs: 60 * 60 * 1000,
//     message: 'Too many requests from this IP! Please try again in an hour.',
//   }),
// );

// routes

app.use('/api/v1/users', userRouter);
app.use('/api/v1/products', productRouter);
app.use('/api/v1/cart', cartRouter);

app.use('/api/v1/stripe', orderRouter);

app.use(errorController);

module.exports = app;
